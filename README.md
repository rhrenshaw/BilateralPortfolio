# Bilateral Portfolio

This project was scaffolded from yo angular generator - https://github.com/yeoman/generator-angular
version 0.11.1.  

Building locally requires NPM, Bower, Ruby and Compass(SaSS) installed and configured.  

## Installation
git clone git@gitlab.com:BobNoel/BilateralPortfolio.git  

npm install  

bower install  

## Build & development
Run `grunt or grunt build` for distribution build to ./dist  

Run `grunt serve` to run from ./.tmp on port9000

## Testing
Running `grunt test` will run the unit tests with karma.  
