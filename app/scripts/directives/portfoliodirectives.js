'use strict';

angular.module('portfolioApp')
.directive("navscroll", function($window) {
    return function(scope) {
        angular.element($window).bind("scroll", function() {
            if (!scope.scrollPosition) {
                scope.scrollPosition = 0;
            }

            if (this.pageYOffset > 300) {
                scope.boolChangeClass = true;
            } else {
                scope.boolChangeClass = false;
            }
            scope.scrollPosition = this.pageYOffset;
            scope.$apply();
        });
    };
})
.directive('scrollPage', function() {
        return {
            restrict: 'A',
            scope: {
                scrollTo: "@"
            },
            link: function(scope, $elm) {

                $elm.on('click', function() {
                    $('html,body').animate({scrollTop: $(scope.scrollTo).offset().top }, "slow");
                });
            }
        };
    }
)
    .directive('iframe', function() {
        return {
            model: {
                changeModal: '@'
            },
            link: function ($scope, element, attrs) {
                console.log(attrs.changemodal);
                if (attrs.changemodal ) {
                    $('.modal-dialog').css('width', attrs.changemodal);
                    //$scope.$digest();
                }

            }
        };
    }
);
