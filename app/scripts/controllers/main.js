'use strict';

angular.module('portfolioApp')
  .controller('MainCtrl', ['$scope', '$rootScope', 'portfolioServices',  'ModalService', '$sce', function ($scope, $rootScope, portfolioServices, ModalService) {

        $scope.portfolioItems = portfolioServices.getPortfolio();

        $scope.showComplex = function(item) {
        $rootScope.modalContent = $scope.portfolioItems.items[item];
        ModalService.showModal({
                templateUrl: "views/modal.html",
                controller: "MainCtrl",
                scope: $scope,
                resolve: {
                    item: function () {
                        return item;
                    }
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(result) {
                    $scope.complexResult  = "Name: " + result.name + ", age: " + result.age;
                });
            });

        };

  }]
);
