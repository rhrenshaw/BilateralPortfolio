'use strict';

angular.module('portfolioApp')
.factory('portfolioServices', function($rootScope, $http) {
    var portfolioService = {};
    portfolioService.items = {};

    portfolioService.getPortfolio = function() {
        $http.get('json/portfolio.json')
            .success(function(data) {
                portfolioService.items = data;
            });

        return portfolioService;
    };

    return portfolioService;


});
